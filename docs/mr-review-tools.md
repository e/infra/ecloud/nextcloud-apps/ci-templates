# MR Review Tools

The MR review tools template is a compilation of different tools and tests template that can help us while working on an MR. The list of tools used can be found `mr-review-tools.yml` file. Currently (as of 2024-09-18), we have the following templates:

| Template file name | Contains | Doc | Notes |
| ------------------ | -------- | --- | ----- |
| `Jobs/SAST.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/user/application_security/sast/) | |
| `Jobs/Dependency-Scanning.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Dependency-Scanning.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) | |
| `Jobs/Code-Quality.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/ci/testing/code_quality.html) | More on Code Climate [here](https://docs.codeclimate.com/docs/getting-started-with-code-climate) |
| `Jobs/Test.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Test.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-test) | This is disabled by default (See [bellow](#test-job)) and it's using [`herokuish buildpack test`](https://github.com/gliderlabs/herokuish) |
| `Jobs/Code-Intelligence.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Intelligence.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/user/project/code_intelligence.html) | This is using [LSIF.dev](https://lsif.dev/) |
| `Jobs/Browser-Performance-Testing.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/ci/testing/browser_performance_testing.html) | |
| `Jobs/Secret-Detection.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/-/blob/v14.0.0-ee/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/user/application_security/secret_detection/) | |
| `Security/DAST.gitlab-ci.yml` | [Link](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml) | [Link](https://docs.gitlab.com/ee/user/application_security/dast/) | |

This template uses `populate-code-climate-default-config.yml` template to populate the `.codeclimate.yml` file in the repo which is needed for the `Jobs/Code-Quality.gitlab-ci.yml` of the `mr-review-tools.yml` file. `populate-code-climate-default-config.yml` in turn depends on `git-operations.yml` template to do the `git commit` of the `.codeclimate.yml` file in the repo. So to add it to any repo:

```yaml
# must have these stages and the jobs from the template uses these
# stages.

stages:
  - test
  - performance
  - dast
  - deploy
  # other needed stages for the job

include:
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "mr-review-tools.yml"
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "git-operations.yml"
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "populate-code-climate-default-config.yml"
```

In the default setting, the template would populate `.codeclimate.yml` file in the repo from `extras/default.codeclimate.yml` of this repo. If we want to override the `.codeclimate.yml` file in the repo, the we must modify the above example to remove the `populate-code-climate-default-config.yml` and `git-operations.yml` templates. The example would look like:

```yaml
# must have these stages and the jobs from the template uses these
# stages.

stages:
  - test
  - performance
  - dast
  - deploy
  # other needed stages for the job

include:
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "mr-review-tools.yml"
```

But we must have a `.codeclimate.yml` file in the repo, we if don't populate it automatically.

### Test Job

We had to disable the `template: Jobs/Test.gitlab-ci.yml` in the `mr-review-tools.yml` template as it expects a `test` script in `package.json` and many of ours projects don't use it. See this [thread](https://gitlab.e.foundation/e/infra/ecloud/nextcloud-apps/hide-my-email/-/merge_requests/26#note_588546) for more context.

In the end we decided that we would disable it by default an add it with `include` for the repos where appropriate. The syntax is same as other `include`:

```yaml
include:
  - template: Jobs/Test.gitlab-ci.yml
```

Or if you already have previous `includes` then:

```yaml
include:
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "nc-apps-lint-build-frontend.yml"
  - template: Jobs/Test.gitlab-ci.yml
```
