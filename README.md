# Ci Templates

this repo holds the CI/CD templates for Nextcloud app deployment.

## Using this template

to use this template add the following in the projects `.gitlab-ci.yml` file.

```
include:
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "nc-apps-checkout-deploy.yml"
  - project: "e/infra/ecloud/nextcloud-apps/ci-templates"
    ref: main
    file: "nc-apps-docker-occ.yml"
```

### CI/CD variables

The following CI/CD variables can be configured for the templates

---

| Type     | Key              | Value(s)          | Notes                                            |
| -------- | ---------------- | ----------------- | ------------------------------------------------ |
| Variable | `ENV_URL`        | https://murena.io | URL of the environment                           |
| Variable | `CONTAINER_NAME` | nextcloud         | Name of the docker container to run the commands |
